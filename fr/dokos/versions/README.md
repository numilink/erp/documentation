# Notes de version

## Version 1

- [v1.0.0](/fr/dokos/versions/v1_0_0.md)
- [v1.1.0](/fr/dokos/versions/v1_1_0.md)
- [v1.2.0](/fr/dokos/versions/v1_2_0.md)
- [v1.3.0](/fr/dokos/versions/v1_3_0.md)
- [v1.4.0](/fr/dokos/versions/v1_4_0.md)

## Version 2

- [v2.0.0](/fr/dokos/versions/v2_0_0.md)