# V1.2.0

:tada: Lire [l'article de blog](https://dokos.io/blog/nouveautes-version-1-2) :tada:

## dokos

#### Fonctionnalités
- Format OFX pour les imports de transactions bancaires
- La clé de paiement est désormais générée avant la validation dans les demandes de paiement, permettant de voir le rendu du modèle de message avant envoi
- Le total net et le total TTC sont désormais simulés pour vous donner un aperçu des montants à facturer dans les abonnements
- Les paramètres d'actifs ont été supprimés. Ce paramètrage se fait désormais par société
- Les contenus des modèles de campagnes d'emailing sont désormais rendus pour une meilleure prévisualisation
- Les réservations d'articles peuvent désormais être synchronisées avec Google Agenda
- Les articles peuvent désormais être liés à un calendrier Google Agenda
- Des articles alternatifs sont désormais proposés si aucun créneau de réservation n'est disponible pour un article sur le portail
- Possibilité de dupliquer un projets avec ses tâches
- Possibilité de différencier les taux de change à l'achat et à la vente

#### Corrections de bug
- Amélioration de l'expérience utilisateur des demandes de paiement
- Amélioration du connecteur Woocommerce
- Amélioration des abonnements GoCardless
- Amélioration de la gestion des webhooks de paiement
- Amélioration du connecteur Shopify
- Amélioration des fiches employés et des notes de frais
- Amélioration de l'outil de mise à jour des listes de matériaux
- Amélioration du plan de production


## dodock [Framework]

#### Fonctionnalités
- Possibilité de désactiver la création automatique des contacts depuis les emails reçus
- Refonte du calendrier (FullCalendar v4)
- Nouveau système de récurrence pour le type de document Evénement (RRULE - RFC5545)
- Les participants aux événements limités au type de document "Contact"
- Possibilité de lier l'intégration Google Agenda avec d'autres types de documents (pas seulement Evénement)
- Possibilité de "grouper" dans les graphiques du tableau de bord
- Le planificateur est désormais automatiquement activé s'il est désactivé lors d'une connexion
- Le panneau ouvert par défaut sur le centre de notification est désormais "Documents ouverts"

#### Corrections de bug
- Corrections dans les répétitions automatiques
- Amélioration du chat
- Sur le portail les utilisateurs peuvent désormais voir uniquement les adresses qu'ils ont créé
- Améliorations de l'outil d'import de données
- Les rapports préparés sont désormais des fichiers privés
- Amélioration du contrôleur de code barre
- Navigation au clavier améliorée dans les lignes de tableaux
