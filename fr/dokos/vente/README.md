# Table des matières

- [Clients](/fr/dokos/vente/client.md)
- [Liste de prix](/fr/dokos/vente/liste-de-prix.md)
- [Prix de l'article](/fr/dokos/vente/prix-article.md)
- [Devis](/fr/dokos/vente/devis.md)
- [Abonnements](/fr/dokos/vente/abonnement.md)