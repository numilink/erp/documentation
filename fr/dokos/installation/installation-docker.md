---
tags: ["installation", "docker"]
---
# Installation Docker

:::warning
Ce type d'installation est encore expérimental.
:::

Cette page explique comment installer Dokos dans un conteneur Docker.  
Cette méthode d'installation est actuellement uniquement disponible sur la branche de développement. La branche master (production) sera bientôt disponible également.  

## Pré-requis

Afin d'installer Dokos sur Docker, vous devez installer les composants suivants sur votre serveur:
- Docker
- Docker-compose
- Git

Vous pouvez trouver plus d'informations sur la manière d'installer `docker` et `docker-compose` sur le [site web de docker](https://docs.docker.com)


## Télécharger Dokidocker

Afin de faciliter l'installation et la maintenance des images Docker, nous avons créé un outil appelé `dokidocker`.  
Cet outil est basé sur le très bon travail fourni par l'équipe de [frappe-docker](https://github.com/frappe/frappe_docker).  
In order to facilitate the installation and the maintenance of the Docker images, we have created a tool called `dokidocker`.  

Afin de télécharger `dokidocker`, lancez la commande suivante sur votre serveur:

```
git clone https://gitlab.com/dokos/dokidocker.git
cd dokidocker
```

## Configurez votre environnement

Avant de lancer vos images docker, vous devez définir un certain nombre de variables d'environnement.  
Commencez par créer un fichier d'environnement en copiant le modèle fourni:

```
cp env-template .env
```

Modifiez ensuite le fichier nouvellement créé pour personnaliser chaque variable d'environnement.  
Vous pouvez utiliser n'importe quel éditeur de texte ou un éditeur intégré à la ligne de commande comme par exemple `nano .env`.  

Les variables par défaut sont:
- **DOKOS_VERSION**: Défini la version à utiliser. `latest` indique la dernière version de développement. Pour les autres tags disponibles, jetez un oeil au [registre](https://gitlab.com/dokos/dokidocker/container_registry/eyJuYW1lIjoiZG9rb3MvZG9raWRvY2tlci9kb2tvcy13b3JrZXIiLCJ0YWdzX3BhdGgiOiIvZG9rb3MvZG9raWRvY2tlci9yZWdpc3RyeS9yZXBvc2l0b3J5LzEyMzM2OTkvdGFncz9mb3JtYXQ9anNvbiIsImlkIjoxMjMzNjk5fQ==)
- **DODOCK_VERSION**: Défini la version à utiliser. `latest` indique la dernière version de développement. Pour les autres tags disponibles, jetez un oeil au [registre](https://gitlab.com/dokos/dokidocker/container_registry/eyJuYW1lIjoiZG9rb3MvZG9raWRvY2tlci9kb2RvY2std29ya2VyIiwidGFnc19wYXRoIjoiL2Rva29zL2Rva2lkb2NrZXIvcmVnaXN0cnkvcmVwb3NpdG9yeS8xMjMyOTU3L3RhZ3M%2FZm9ybWF0PWpzb24iLCJpZCI6MTIzMjk1N30=)
- **MARIADB_HOST**: Nom de domaine pour MariaDB. Gardez `mariadb` si vous utilisez le conteneur de base de donnée par défaut.
- **MYSQL_ROOT_PASSWORD**: Mot de passe pour accéder à la base de données MariaDB dans un conteneur. Si vous utilisez une base de données MariaDB gérée ou externe, vous n'avez pas besoin de définir de mot de passe ici.
- **SITE_NAME**: Site créé après le lancement des conteneurs. Le nom du site doit être un nom de domaine/sous-domaine complet, par exemple `erp.example.com` ou `erp.localhost`
- **SITES**: Liste des sites inclus dans ce déploiement. Si Let's Encrypt est configuré, assurez-vous que vos paramètres DNS pointent correctement vers le serveur actuel.
- **DB_ROOT_USER**: Nom d'utilisateur Root pour MariaDB.
- **ADMIN_PASSWORD**: Mot de passe administrateur pour Dodock/Dokos.
- **INSTALL_APPS**: Applications contenues dans l'image utilisée et qui doivent être installées sur le site défini dans SITE_NAME.
- **ENTRYPOINT_LABEL**: Configuration Traefik. Pour un déploiement local, vous devez mettre `web`, pour un déploiement en production `websecure`.
- **CERT_RESOLVER_LABEL**: Resolver Traefik a utiliser pour obtenir un certificat TLS. Utilisez `dodock.local.no-cert-resolver` pour un déploiement local.
- **LETSENCRYPT_EMAIL**: Adresse email pour les notifications d'expiration LetsEncrypt.
- **HTTPS_REDIRECT_RULE_LABEL**: Configuration pour la redirection HTTPS par Traefik. Utilisez `dodock.local.no-redirect-rule` pour un déploiement local.
- **HTTPS_REDIRECT_ENTRYPOINT_LABEL**: Configuration pour la redirection HTTPS par Traefik. Utilisez `dodock.local.no-entrypoint` pour un déploiement local.
- **HTTPS_REDIRECT_MIDDLEWARE_LABEL**: Configuration pour la redirection HTTPS par Traefik. Utilisez `dodock.local.no-middleware` pour un déploiement local.
- **HTTPS_USE_REDIRECT_MIDDLEWARE_LABEL**: Configuration pour la redirection HTTPS par Traefik. Utilisez `dodock.local-no-redirect-middleware` pour un déploiement local.


## Démarrer les conteneurs

Afin de démarrer Dokos, choisissez un nom de projet et lancez la commande suivante:

`docker-compose --project-name <nom-du-projet> up -d`

## Mettre à jour le site

Depuis le dossier `dokidocker`, commencez par modifier les versions de Dodock et Dokos en modifiant les valeurs de __DODOCK_VERSION__ et __DOKOS_VERSION__.
Si vous utilisez des images qui ne référencent pas une version précise, cette étape n'est pas nécessaire.

`nano .env`

Télécharger les nouvelles images
`docker-compose pull`

Et redémarrez vos conteneurs
`docker-compose --project-name <nom-du-projet> up -d`

## Notes

- La première installation peut prendre plusieurs minutes.
Vous pouvez utiliser la commande `docker logs <nom-du-projet>_site-creator_1 -f` pour suivre son avancement.
- Après la création du site, vous pouvez y accéder en utilisant `Administrator` comme nom d'utilisateur et le mot de passe défini en face de la variable __ADMIN_PASSWORD__.
- Les noms de site locaux sont limités aux modèles correspondant à *.localhost par défaut.  
D'autres modèles de noms peuvent être ajoutés en modifiant /etc/host sur votre machine.  
