# Modèles d'adresses

Un modèle d'adresse est un format d'affichage des différents éléments composants une adresse.

## 1. Pourquoi des modèles d'adresses

L'affichage des adresses étant différent en fonction des pays, il faut pouvoir afficher une adresse différemment si l'on envoie une facture en France, en Allemagne ou en Chine.

Pour cette raison, dokos permet de configurer un modèle d'adresse par pays et de définir un de ces modèles comme étant le modèle à utiliser par défaut (s'il n'existe pas de modèle pour un pays donnée).


## 2. Créer un modèle

Pour créer un nouveau modèle allez dans `Paramètres > Modèle d'adresse` et cliquez sur nouveau.

1. Choisissez le pays pour lequel appliquer ce modèle.
1. Cochez la case `Modèle par défaut` si vous souhaitez que ce modèle soit utilisé par défaut sur votre système.
1. Ajoutez un modèle au format [Jinja](http://jinja.pocoo.org/docs/templates/) ou laissez vide pour que le système ajoute un modèle standard lors de l'enregistrement.

## 3. Modifier un modèle


Voici le modèle standard:

```
{{ address_line1 }}<br>{% if address_line2 %}{{ address_line2 }}<br>{% endif -%}{{ city }}<br>
{% if state %}{{ state }}<br>{% endif -%}
{% if pincode %}{{ pincode }}<br>{% endif -%}
{{ country }}<br>
{% if phone %}Téléphone: {{ phone }}<br>{% endif -%}
{% if fax %}Fax: {{ fax }}<br>{% endif -%}
{% if email_id %}Email: {{ email_id }}<br>{% endif -%}
```

Notre addresse fictive ne comportant ni champ `address_line_2` ni champ `fax`, cela donne:

```
35 rue de la Paix
Paris
75001
Téléphone: 06.12.34.56.78
Email: hello@exemple.com
```


Par contre nous souhaitons faire apparaître le code postal à gauche du nom de la ville et supprimer le champ `state` qui n'existe pas en France.
Il faut donc modifier votre modèle de la façon suivante:

```
{{ address_line1 }}<br>{% if address_line2 %}{{ address_line2 }}<br>{% endif -%}
{% if pincode %}{{ pincode }} {% endif -%}
{{ city }}<br>
{{ country }}<br>
{% if phone %}Téléphone: {{ phone }}<br>{% endif -%}
{% if fax %}Fax: {{ fax }}<br>{% endif -%}
{% if email_id %}Email: {{ email_id }}<br>{% endif -%}
```

Le résultat sera:

```
35 rue de la Paix
75001 Paris
Téléphone: 06.12.34.56.78
Email: hello@exemple.com
```

Tous les champs du type de document `Adresse` sont accessibles dans un modèle d'addresse. Même les champs personnalisés.