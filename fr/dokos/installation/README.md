---
tags: ["installation"]
---
# Installation
<TagLinks />

- Il s'agit d'une installation automatique destinée à être installée sur un serveur vierge.
- Fonctionne avec Ubuntu 18.04+, CentOS 7+, Debian 8+
- Il est nécessaire d'installer Python 3.6+ en lançant `apt-get install python3-minimal`
- Vous devez également installer les librairies build-essential et python-setuptool en lançant `apt-get install build-essential python3-setuptools`
- Ce script va installer les pré-requis, installer bench et créer un nouveau site dokos
- Vous devrez choisir un mot de passe pour l'administrateur système et pour MariaDB (utilisateur root)
- Vous pourrez ensuite vous connecter avec l'utilisateur **Administrator** et le mot de passe choisi.

Ouvrez un terminal sur votre serveur et lancez:

#### 1. Création d'un utilisateur

Si vous êtes sur un serveur vierge et connecté en temps que **root**, créez d'abord un nouvel utilisateur pour dokos et donnez lui les droits **sudo**:

```
  adduser [dokos-user]
  usermod -aG sudo [dokos-user]
```

Puis connectez-vous avec cet utilisateur

    su - [dokos-user]


#### 2. Téléchargement du script d'installation

Pour Linux:

    wget https://gitlab.com/dokos/docli/raw/master/playbooks/install.py


#### 3. Lancement du script d'installation

Lancez le script pour installer dokos en mode production:

    sudo python3 install.py --production --user [dokos-user]


::: tip Astuce
Les applications s'appellent Frappe et ERPNext car dokos est un adaptation de ces logiciels.
L'architecture sous-jacente est similaire à celle de ces deux logiciels.
Vous trouverez plus d'information sur leurs sites respectifs: [Frappe](https://frappe.io/docs), [ERPNext](https://erpnext.com/docs)
:::

#### Que fait ce script ?

- Installation des pré-requis
- Installation de l'outil de ligne de commande `bench`
- Création d'un nouveau bench (un dossier contenant votre ou vos sites dokos)
- Création d'un nouveau site dokos

#### Comment démarrer dokos

Votre site sera automatiquement configuré et gérér par `nginx` et `supervisor`.
Si ce n'est pas le cas, vous pouvez lancer depuis le dossier bench:

    sudo bench setup production [dokos-user]


Vous pouvez alors vous connecter à l'adresse de votre serveur pour commencer à utiliser dokos.

::: tip Astuce
Si votre serveur est à l'adresse 57.69.123.1, connectez-vous à cette adresse pour accéder à votre site.
:::

---

Aide
====

Pour l'aide de bench, vous pouvez lancer

    bench --help

Mise à jour
========

Pour mettre à jour dokos, lancez `bench update` depuis votre dossier bench.
Cela mettra à jour les applications, les patches, cela compilera les fichiers JS et CSS et redémarrera supervisor.
To manually update the bench, run `bench update` to update all the apps, run

Vous pouvez aussi lancer une partie du script de mise à jour avec les commandes suivantes:

`bench update --pull` récupèrera les mises à jour du code des applications

`bench update --patch` lancera la migration de la base de données vers une nouvelle version

`bench update --build` compilera les fichiers JS et CSS pour ce dossier bench

`bench update --bench` mettra à jour l'outil de ligne de commande bench

`bench update --requirements` mettra à jour les librairies dont dépendent les applications installées