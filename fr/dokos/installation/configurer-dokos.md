---
tags: ["configurer", "sécurité"]
---
# Configurer dokos

Une fois que vous avez installé _Dokos_ vous voudrez certainement activer sa fonctionnalité DNS Multitenant and sécuriser votre site avec [Let's encrypt](https://letsencrypt.org/fr/).

## DNS Multitenant

### Multi-tenant basé sur les ports

Par défaut un port spécifique de votre serveur est attribué à votre nouveau site.
Le port 80 est attribué au premier, le port 81 au deuxième, etc...

Pour modifier le port attribué à un site, vous pouvez lancer:
`bench set-nginx-port {votre site} {numéro de port}`

Par exemple:
`bench set-nginx-port siteexemple.com 81`


### Multi-tenant basé sur l'adresse DNS

Afin de simplifier la gestion de votre serveur web, vous pouvez activer la fonctionnalité DNS Multitenant.
Cette fonctionnalité vous permet de simplement nommer le dossier contenant votre site comme votre nom de domaine et de laisser _Dodock_ gérer le reste:

Pour activer le mode DNS Multitenant, lancez:
`bench config dns_multitenant on`

Si vous avez déjà créer un site, vous pouvez le renommer avec:
`mv sites/{ancien nom du site} sites/{nouveau nom du site}`

### Créer un nouveau site

Pour créer un nouveau site, vous pouvez lancer:
`bench new-site {votre site}`

### Relancez le serveur web

Une fois que vous avez modifié le port, activé le mode DNS Multitenant ou créé un nouveau site, vous devez mettre à jour la configuration de Nginx (serveur web).

1. Regénérez le fichier de configuration: `bench setup nginx`
2. Rechargez Nginx: `sudo systemctl reload nginx` ou `sudo service nginx reload`


## Let's encrypt

Vous pouvez amener votre propre certificat pour sécuriser votre installation _Dokos_ ou générer un nouveau certificate en utilisant Let's Encrypt.

### Pré-requis

1. Votre bench doitêtre en mode DNS Multitenant
2. Votre domaine web doit rediriger vers le serveur de votre site
3. Vous devez pouvoir exécuter les commandes avec des droits administrateur (root)

### Générez un nouveau certificat

`sudo -H bench setup lets-encrypt {votre site}`

### Générez un nouveau certificat pour un domain personnalisé

`sudo -H bench setup lets-encrypt {votre site} --custom-domain {votre domaine personnalisé}`

### Renouveler un certificat

Chaque certificat Let's Encrypt est valide pendant 3 mois. Vous pouvez les renouveler 30 jours avant qu'ils expirent.
La génération d'un nouveau domaine crée automatiquement une tâche cron qui tente de renouveler le certificat automatiquement.
Si ce renouvelement automatique ne fonctionne pas, vous pouvez quand même renouveler votre certificat manuellement en lançant:

`sudo bench renew-lets-encrypt`