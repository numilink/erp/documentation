# Réservation d'articles

**La réservation d'articles est un document flexible permettant d'enregistrer des réservation de créneaux horaires pour un article.**

### 1. Usage

Vous pouvez utiliser le document de réservation d'articles de deux façons:
1. Vous pouvez faire une nouvelle réservation manuellement depuis le bureau.
2. Vous pouvez configurer le document de réservation d'articles pour autoriser vos utilisateurs de site web à réserver un créneau eux-mêmes.


### 2. Pré-requis

#### 2.1. Paramètres des lieux

##### Unité de mesure pour 1 minute

Si vous voulez autoriser des invités à sélectionner un créneau disponible sur votre site web, vous devez configurer l'unité de mesure correspondant à une minute dans la section "Réservation d'articles" des paramètres des lieux.
Tous les créneaux étant des multiples d'une minutes, il faut indiquer cette unité de mesure à __Dokos__.

##### Autoriser les réservations simultanées

Option permettant d'autoriser les réservations simultanées d'articles (plusieurs réservation d'un même article pendant le même créneau).
Cela active la possibilité d'indiquer un nombre de réservations simultanées autorisées pour chaque article dans les fiche d'articles.

##### Supprimer les réservations dans le panier après x minutes

Paramétrage permettant de définir l'intervale de temps entre la dernière modification d'une réservation d'articles en statut "Dans le panier" et sa suppression automatique par le logiciel. Mettez 0 pour désactiver cette fonctionnalité.

#### 2.2. Unité de mesure

Afin que le système soit capable de convertir les unités de mesure de vente en minutes pour calculer les créneaux disponibles, vous devez configurer un ou plusieurs facteurs de conversion d'unité de mesure.

Dans le type de document __Facteur de conversion de l'unité de mesure__, vérifiez que vous avez un facteur de conversion pour toutes vos unités de mesure de vente et de stock pour une minute:

|De        |Vers    |Valeur           |
|------------|------|----------------|
|UdM de vente|Minute|Minute/UdM de vente|

![Conversion du temps en minutes](/images/venue/item_booking/time_conversion.png)

:::tip Exemple
Si vous prévoyez de vendre des créneaux à l'heure ou à la journée, assurez-vous d'avoir au moins ces deux facteurs de conversion:

|De|Vers    |Valeur|
|----|------|-----|
|Heure|Minute|60   |
|Jour |Minute|480  |

Veuillez noter qu'ici, 1 jour correspond à 480 minutes, car nous considérons que, d'un point de vue commercial, nous vendons 8 heures par jour.
Vous pouvez, bien sûr, ajuster cette valeur en fonction de vos besoins métiers.
:::

#### 2.3. Calendrier de réservation d'articles

Vous pouvez configurer autant de calendrier de réservation que vous le souhaitez par articles et unités de mesures.
La règle de sélection du calendrier correspondant à une réservation d'article est la suivante:

1. __Dokos__ cherche un calendrier correspondant à l'article et l'unité de mesure demandés
2. __Dokos__ cherche un calendrier correspondant à l'article demandé, sans unité de mesure
3. __Dokos__ cherche un calendrier correspondant à l'unité de mesure demandée, sans article
4. __Dokos__ cherche un calendrier lié à aucun article et aucune unité de mesure

Il est donc utile de configurer au moins un calendrier qui ne soit lié ni à un article ni à une unité de mesure, pour qu'il puisse servir de calendrier par défaut.


#### 2.4. Article

Pour pouvoir autoriser la réservation de créneaux horaires sur votre site web, vous devez d'abord afficher l'article sur votre site web: dans la section __Site web__, sélectionnez __Afficher sur le site web__.  
Activer cette option vous permettra aussi d'__Autoriser la réservation d'articles__.

Une fois activée, vos utilisateurs de site web auront le choix entre acheter des unités ou sélectionner un créneau:  
![Options de réservation d'article sur le site web](/images/venue/item_booking/item_website_options.png)

Vous pouvez cocher la case __Désactiver l'achat d'unités__ pour ne permettre que la réservation de créneaux.

__Réservations simultanées autorisées__: Définissez le nombre de réservations simultanées autorisées (Après activation dans les paramètres des lieux)

Si vous souhaitez autoriser la réservation avec différentes unités de mesure (jour, heure,...) ajoutez les dans le tableau des unités de mesure.
Notez que la conversion pour les réservations se fera selon les facteurs de conversions pour une minute définis ci-dessus.

### 3. Réservation d'article

Vos utilisateurs de site web ont accès à un popup listant tous les créneaux disponibles.
Ils peuvent cliquer sur un créneau disponible et l'ajouter dans leur panier.


![Popup de réservation](/images/venue/item_booking/item_booking_dialog.png)


Les réservations en brouillon (non validées) sont automatiquement supprimées toutes les x minutes (définies dans les paramètres des lieux) pour les cas d'abandon de panier.

### 4. Réservations depuis un événement

Si vous organisez des événements que vous souhaitez gérer depuis le calendrier "Evénement", il est possible de lier vos réservations d'articles et vos événements.

> Vous organisez une exposition temporaire, nécessitant la réservation de votre grande salle, de la cuisine et de deux salles de réunions pendant toute la durée de l'événement.
> Créez votre événement et réservez vos salles depuis le bouton "Réserver un article" de la fiche Evénement.
> Vous pouvez ensuite modifier les horaires et conditions de réservation individuellement depuis chaque réservation d'article.

#### 5. Portail

Vous pouvez activer le portail "Bookings" pour donner un accès à vos clients à la liste des créneaux qu'ils ont réservés.  
Les différentes réservations apparaîtront avec les statuts "Confirmé", "Annulé" ou "Passé".

Si vous donnez l'autorisation d'écrire dans une réservation d'articles à vos clients, ceux-ci pourront également voir un bouton `Annuler` leur permettant d'annuler leur réservation.
Cette autorisation peut être donnée en cochant la case `Ecrire` pour le rôle `Client` (par défaut) dans le gestionnaire des rôles et autorisation.

#### 6. Intégration Google Agenda

Il est possible de synchroniser vos réservations d'article avec Google Agenda.
Pour paramétrer la connexion et créer des agendas, lire cette [section](/fr/dokos/installation/integrations/google).

##### 6.1. Configuration

Vous pouvez le lier chaque article avec un agenda ayant comme document de référence **Réservation d'articles** dans la section **Google Agenda**
Si un article n'est pas lié avec un agenda, il faudra sélectionner celui-ci manuellement dans chaque réservation.

:::warning Attention
Chaque article doit être lié avec un agenda différent.
:::

##### 6.2. Synchronisation

Pour synchroniser vos réservations, cochez la case **Synchroniser avec Google Agenda**.
Vérifiez qu'un agenda est bien sélectionné dans le champs **Google Agenda**.

:::warning Attention
__dokos__ ne gère pas encore les exceptions liées aux récurrences d'événement.
Si vous souhaitez supprimer un événement récurrent dans Google Agenda, sélectionnez bien l'option **Tous les événements**.
Si vous supprimez seulement une occurence de cet événement, cela n'apparaîtra pas dans __dokos__.
:::

Pour automatiser la synchronisation des réservations en ligne, cochez la case **Synchroniser automatiquement avec Google Calendar** dans les **paramètres des lieux**.  

Si vous souhaitez que les réservations d'articles soient toujours synchronisées, personnalisez le formulaire de réservation d'articles et mettez 1 comme valeur par défaut pour le champs **Synchroniser avec Google Agenda**.

