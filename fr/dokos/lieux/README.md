# Module Lieu

Le module Lieu a été créé pour regrouper les principales fonctionnalités permettant de gérer un lieu.  

# Table des matières

- [Réservation d'articles](/fr/dokos/lieux/reservation-articles.md)
- [Crédits de réservation](/fr/dokos/lieux/credits-reservations.md)