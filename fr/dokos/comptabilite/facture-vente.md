# Facture de vente

Un facture de vente est un document que vous envoyez à vos clients en échange d'un paiement.  
C'est également un transaction comptable: A la validation d'une facture, le système met à jour les comptes clients et enregistre un revenu.

### 1. Créer une facture de vente

1. Allez à: **Comptabilité > Comptes débiteurs > Facture de vente > Nouveau·elle**.
2. Sélectionnez un client. 
3. Définissez une date d'échéance de paiement.
4. Dans la table d'articles, sélectionnez des articles et leurs quantités respectives.
5. Les prix sont récupérés automatiquement.
6. La date de comptabilisation est la date du jour, mais vous pouvez la modifier en cliquant sur le bouton juste en dessous.
7. Vous pouvez également faire un avoir en cliquant sur la case correspondante.
8. Enregistrez et validez.

Pour récupérer des détails depuis d'autres documents, cliquez sur __Obtenir les articles de__.  
Les détails peuvent être récupérés depuis une commande client, un bon de livraison ou un devis.


### 2. Fonctionnalités
#### 2.1 Dates

__Date de comptabilisation__: La date à laquelle la facture sera comptabilisée dans votre grand livre.
Votre balance comptable sera impactée à cette date.

__Date d'échéance__: Date à laquelle le paiement est dû.  
Si la date d'échéance n'est pas passée, la facture aura le statut "Impayé.  
Après la date d'échéance, elle sera considérée comme "En retard".

#### 2.2 Récupération automatique des numéros de lots par article

Si vous vendez un article d'un lot, dokos va récupérer automatiquement un numéro de lot pour vous si  "Mettre à jour le stock" est coché.  
Le numéro de lot est récupéré sur une base FEFO (Premier expiré premier sorti).  
Il s'agit d'une variante de FIFO (Premier entré premier sorti) qui donne la priorité aux articles les plus proches de leur date d'expiration.

Veuillez noter que le premier lot dans la file d'attente ne peut remplir les conditions de commande la facture, le système sélectionne le premier lot répondant à ces conditions dans la liste.  
Si aucun lot ne peut répondre aux conditions de la commande, dokos annulera la récupération automatique de numéro de lot pour cette facture.

#### 2.4 Factures de point de vente

Dans un scénario d'utilisation du point de vente, la case __Est payé__ est cochée automatiquement lors du règlement de la facture.  
Les données du __Profil de point de vente__ sont récupérées pour compléter la facture et le paiement est enregistré directement dans la facture. 
Si le stock doit être mis à jour, la case __Mettre à jour le stock__ est cochée automatiquement.  


#### 2.5 Facturer des feuilles de temps liées à un projet

Si vous voulez facturer les heures travaillées sur un projet par vos employés, ils doivent remplir des feuilles de temps contenant leur taux de facturation.  
Lorsque vous faites une nouvelle facture, sélectionnez le projet que vous souhaitez facturer et le système récupérera automatiquement les feuilles de temps correspondantes.
Ajoutez l'article que vous souhaitez facturer et reportez le montant à facturer dans la ligne d'article correspondante.  


#### 2.6 Facture "Pro Forma"

Si vous souhaitez envoyer une facture à client pour qu'il puisse effectuer un paiement avant la livraison, vous pouvez créer un devis ou une commande client et l'imprimer avec le titre "Facture Pro-forma" grâce au choix du __Titre d'impression__.

### 3. Aller plus loin
#### Impact comptable

Toutes les ventes doivent être comptabilisées en créditant un compte de produits (+ le compte de TVA le cas échéant) et en débitant un compte client.
Le compte de produits est déterminé en fonction de l'article sélectionné, il est donc primordial de bien configurer vos articles en leur affectant le bon compte de produits avant de faire votre transaction.

:::tip
Vous pouvez également définir le compte de produits au niveau d'un groupe d'article ou de la société si vous n'en utilisez qu'un seul pour toutes vos transactions.
:::

L'autre compte est le compte client, qui est défini dans la fiche du client ou dans la fiche de la société si vous utilisez le même pour tous vos clients.  
Dans dokos, vous n'avez pas besoin de créer un compte pour chaque client, le système fonctionne grâce à l'enregistrement des transactions en face de comptes auxiliaires correspondant à chaque client. 

Vous pouvez aussi enregistrer vos transactions en les liant à un centre de coût.  
Cela n'a pas d'impact comptable, mais vous permet de générer un compte de résultat et un bilan pour chaque centre de coût et de calculer la profitabilité de plusieurs unités de vente.

#### Ecritures comptables (Ecritures de grand livre) pour une vente classique:

|**Débit**|**Crédit:**|
|---------|-----------|
| - Client (Total TTC) | - Compte de produits (Total HT pour chaque article) <br>- TVA|


> Pour voir les écritures comptables générées lors de la validation de votre facture, cliquez sur "Voir > Livre des comptes"