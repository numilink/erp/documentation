# Ecriture de paiement

Une écriture de paiement est un document enregistrant un paiement destiné à/ou provenant d'un tiers.

### 1. Comment créer une écriture de paiement

1. Dans une commande, facture de vente ou facture d'achat validée, mais impayée, cliquez sur 'Créer > Ecriture de paiement'
2. Vérifiez le montant payé
3. Vérifiez l'allocation au document de référence (commande client, facture de vente, etc...)
4. Ajoutez une référence et une date de référence
5. Enregistrez et validez


### 2. Comment enregistrer des frais associés à un paiement

Le tableau de déductions ou pertes permet d'enregistrer tout type de frais associés à un paiement.

:::tip Exemple
Une facture d'achat de $40 a été comptabilisée pour un montant de 36€ dans la devise de la société.
Le paiement, plusieurs jours plus tard, est finalement de 37€.
Dans l'écriture de paiement, le montant payé sera de 37€, le montant alloué à la facture de 36€ et dans le tableau de déductions on ajoute la ligne suivante:

Compte: 666 - Pertes de change
Montant: 1€
:::


:::tip Exemple
Une facture de vente de 1000€ est payée via GoCardless, le montant réellement reçu est donc de 998€.
Le montant payé dans l'écriture de paiement est donc de 998€
Le montant alloué à la facture est de 1000€
Dans le tableau de déduction on ajoute la ligne suivante:

Compte: 6XXXXX - Frais GoCardless
Montant: 2€
:::