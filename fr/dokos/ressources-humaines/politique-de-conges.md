# Politique de congés

Une politique de congés défini les règles applicables à une catégorie d'employés.

## Allocations de congés

Dans le tableau des allocations ajoutez une nouvelle ligne pour chaque type de congés et définissez l'allocation annuelle de congés applicable pour ce type de congés.  

## Liens

La politique de congés peut être affectée à chaque employé directement ou à un échelon.  
Si vous l'affectez à un échelon, n'oubliez d'affecter cet échelon à tous les employés concernés.  