# Table des matières

- [Types de congés](/fr/dokos/ressources-humaines/types-de-conges.md)
- [Politique de congés](/fr/dokos/ressources-humaines/politique-de-conges.md)
- [Période de congés](/fr/dokos/ressources-humaines/periode-de-conges.md)