module.exports = [
	{
		text: 'dokos',
		link: '/dokos/',
		items: [
			{
				text: 'Quick Start',
				items: [
					{
						text: 'Install dokos',
						link: '/dokos/setting-up/'
					},
					{
						text: 'Developers',
						link: '/dokos/developers/'
					},
					{
						text: 'Contributing',
						link: '/dokos/contributing/'
					}
				]
			},
			{
				text: 'Modules',
				items: [
					{
						text: 'Accounting',
						link: '/dokos/accounting/'
					},
					{
						text: 'Buying',
						link: '/dokos/buying/'
					},
					{
						text: 'Human Resources',
						link: '/dokos/human-resources/'
					},
					{
						text: 'Selling',
						link: '/dokos/selling/'
					},
					{
						text: 'Stocks',
						link: '/dokos/stocks/'
					},
					{
						text: 'Venue',
						link: '/dokos/venue/'
					},
					{
						text: 'Website',
						link: '/dokos/website/'
					}
				]
			},
			{
				text: 'Releases',
				items: [
					{
						text: 'Versions',
						link: '/dokos/versions/'
					}
				]
			}
		]
	}
]