module.exports = [
	{
		text: 'dokos',
		link: '/fr/dokos/',
		items: [
			{
				text: 'Démarrer avec dokos',
				items: [
					{
						text: 'Installer dokos',
						link: '/fr/dokos/installation/'
					},
					{
						text: 'Developpeurs',
						link: '/fr/dokos/developpeurs/'
					},
					{
						text: 'Contribuer',
						link: '/fr/dokos/contribuer/'
					}
				]
			},
			{
				text: 'Modules',
				items: [
					{
						text: 'Achats',
						link: '/fr/dokos/achats/'
					},
					{
						text: 'Comptabilité',
						link: '/fr/dokos/comptabilite/'
					},
					{
						text: 'Lieux',
						link: '/fr/dokos/lieux/'
					},
					{
						text: 'Ressource Humaines',
						link: '/fr/dokos/ressources-humaines/'
					},
					{
						text: 'Site web',
						link: '/fr/dokos/site-web/'
					},
					{
						text: 'Stocks',
						link: '/fr/dokos/stocks/'
					},
					{
						text: 'Vente',
						link: '/fr/dokos/vente/'
					}
				]
			},
			{
				text: 'Publications',
				items: [
					{
						text: 'Versions',
						link: '/fr/dokos/versions/'
					}
				]
			},
		]
	}
]