module.exports = function getSidebar (lang) {
	if (lang === 'fr') {
		return [
			{
				title: 'Lieux',
				collapsable: false,
				children: [
					'',
					'reservation-articles',
					'credits-reservations'
				]
			}
		]
	}

	return [
		{
			title: 'Venue',
			collapsable: false,
			children: [
				'',
				'item-booking',
				'booking-credits'
			]
		}
	]
}