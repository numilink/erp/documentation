module.exports = function getSidebar (lang) {
	if (lang === 'fr') {
		return [
			{
				title: 'API',
				collapsable: false,
				children: [
					'api/api-documents-python',
					'api/api-base-de-donnee-python',
					'api/api-jinja-python',
					'api/api-rest'
				]
			}
		]
	}

	return [
		{
			title: 'API',
			collapsable: false,
			children: [
				'api/python-documents-api',
				'api/python-database-api',
				'api/python-jinja-api',
				'api/rest-api'
			]
		}
	]
}