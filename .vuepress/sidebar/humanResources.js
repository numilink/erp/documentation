module.exports = function getSidebar (lang) {
	if (lang === 'fr') {
		return [
			{
				title: "Ressources Humaines",
				collapsable: false,
				children: [
					'',
					'type-de-conges',
					'politique-de-conges',
					'periode-de-conges'
				]
			}
		]
	}

	return [
		{
			title: "Human Resources",
			collapsable: false,
			children: [
				'',
				'leave-type',
				'leave-policy',
				'leave-period'
			]
		}
	]
}