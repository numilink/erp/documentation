module.exports = function getSidebar (lang) {
	if (lang === 'fr') {
		return [
			{
				title: 'Démarrage',
				collapsable: false,
				children: [
					'',
					'migration-erpnext',
					'installation-docker',
					'mettre-a-jour-dokos',
					'configurer-dokos'
				]
			},
			{
				title: 'Configurer dokos',
				collapsable: false,
				children: [
					'configuration/configuration-email',
					'configuration/formats-impression',
					'configuration/modele-adresses'
				]
			},
			{
				title: 'Personnaliser dokos',
				collapsable: false,
				children: [
					'personnalisation/modeles-jinja',
					'personnalisation/scripts-python'
				]
			},
			{
				title: 'Intégrations',
				collapsable: false,
				children: [
					'integrations/google',
					'integrations/zapier'
				]
			}
		]
	}
	return [
		{
			title: 'Setup',
			collapsable: false,
			children: [
				'',
				'erpnext-migration',
				'docker-installation',
				'updating-dokos',
				'configuring-dokos'
			]
		},
		{
			title: 'Configure dokos',
			collapsable: false,
			children: [
				'configuration/email-setup',
				'configuration/print-formats',
				'configuration/address-templates'
			]
		},
		{
			title: 'Customize dokos',
			collapsable: false,
			children: [
				'customization/jinja-templates',
				'customization/server-scripts'
			]
		},
		{
			title: 'Integrations',
			collapsable: false,
			children: [
				'integrations/google',
				'integrations/zapier'
			]
		}
	]
}