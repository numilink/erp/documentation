module.exports = function getSidebar (name, lang) {
	if (lang === 'fr') {
		return [
			{
				title: name,
				collapsable: false,
				sidebarDepth: 2,
				children: [
					'',
					'parametres-du-site-web',
					'formulaire-web'
				]
			}
		]
	}

	return [
		{
			title: name,
			collapsable: false,
			sidebarDepth: 2,
			children: [
				'',
				'website-settings',
				'web-form'
			]
		}
	]
}