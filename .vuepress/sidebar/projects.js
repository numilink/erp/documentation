module.exports = function getSidebar (name, lang) {
	if (lang === 'fr') {
		return [
			{
				title: name,
				collapsable: false,
				children: [
					''
				]
			}
		]
	}

	return [
		{
			title: name,
			collapsable: false,
			children: [
				''
			]
		}
	]
}