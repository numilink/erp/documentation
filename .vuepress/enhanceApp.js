import TagLinks from './components/TagLinks.vue'
import TagList from './components/TagList.vue'

export default ({
	Vue, // the version of Vue being used in the VuePress app
	options, // the options for the root Vue instance
	router, // the router instance for the app
	siteData // site metadata
	}) => {
		Vue.component('TagLinks', TagLinks)
		Vue.component('TagList', TagList)
	}