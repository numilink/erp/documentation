const getSettinUpSidebar = require('./sidebar/settingUp.js')

const fs = require("fs");
const path = require("path");

module.exports = {
	title: 'dokos',
	description: 'Documentation for dokos',
	locales: {
		'/': {
		lang: 'en-US',
		title: 'dokos',
		description: 'Open-source platform for SME'
		},
		'/fr/': {
		lang: 'fr-FR',
		title: 'dokos',
		description: 'Plateforme open-source pour PME'
		}
	},
	head: [
		['link', { rel: 'icon', href: `/favicon.ico` }],
		['link', { rel: 'manifest', href: '/manifest.json' }],
		['meta', { name: 'theme-color', content: '#3eaf7c' }],
		['meta', { name: 'apple-mobile-web-app-capable', content: 'yes' }],
		['meta', { name: 'apple-mobile-web-app-status-bar-style', content: 'black' }],
		['link', { rel: 'apple-touch-icon', href: `/icons/apple-touch-icon-152x152.png` }],
		['link', { rel: 'mask-icon', href: '/icons/safari-pinned-tab.svg', color: '#3eaf7c' }],
		['meta', { name: 'msapplication-TileImage', content: '/icons/msapplication-icon-144x144.png' }],
		['meta', { name: 'msapplication-TileColor', content: '#000000' }]
	],
	themeConfig: {
		logo: '/logo/dokos_logo_rect.svg',
		repo: 'https://gitlab.com/dokos/documentation',
		editLinks: true,
		algolia: {
			apiKey: 'ae1fcf2104324e91009179abc4b96849',
			indexName: 'dokos'
		},
		locales: {
			'/': {
				label: 'English',
				lastUpdated: 'Last Updated',
				selectText: 'Languages',
				editLinkText: 'Edit this page on Gitlab',
				lastUpdated: 'Last Updated',
				nav: require('./nav/en'),
				sidebar: {
					'/dokos/setting-up/': getSettinUpSidebar('en'),
					'/dokos/developers/': getSideBar('developers', "Developers", 'en', ['api']),
					'/dokos/stocks/': getSideBar('stocks', "Stocks", 'en'),
					'/dokos/venue/': getSideBar('venue', "Venue", 'en'),
					'/dokos/website/': getSideBar('website', "Website", 'en'),
					'/dokos/accounting/': getSideBar('accounting', "Accounting", 'en'),
					'/dokos/selling/': getSideBar('selling', "Selling", 'en'),
					'/dokos/buying/': getSideBar('buying', "Buying", 'en'),
					'/dokos/versions/': getSideBar('versions', "Versions", 'en'),
					'/dokos/contributing/': getSideBar('contributing', "Contributing", 'en'),
					'/dokos/human-resources/': getSideBar('human-resources', "Human Resources", 'en')
				}
			},
			'/fr/': {
				label: 'Français',
				lastUpdated: 'Dernière mise à jour',
				selectText: 'Langues',
				editLinkText: 'Modifier cette page sur Gitlab',
				lastUpdated: 'Dernière mise à jour',
				nav: require('./nav/fr'),
				sidebar: {
					'/fr/dokos/installation/': getSettinUpSidebar('fr'),
					'/fr/dokos/developpeurs/': getSideBar('developpeurs', "Développeurs", 'fr', ['api']),
					'/fr/dokos/stocks/': getSideBar('stocks', "Stocks", 'fr'),
					'/fr/dokos/lieux/': getSideBar('lieux', "Lieu", 'fr'),
					'/fr/dokos/site-web/': getSideBar('site-web', "Website", 'fr'),
					'/fr/dokos/comptabilite/': getSideBar('comptabilite', 'Comptabilité', 'fr'),
					'/fr/dokos/vente/': getSideBar('vente', 'Vente', 'fr'),
					'/fr/dokos/achats/': getSideBar('achats', 'Achats', 'fr'),
					'/fr/dokos/versions/': getSideBar('versions', 'Versions', 'fr'),
					'/fr/dokos/contribuer/': getSideBar('contribuer', 'Contribuer', 'fr'),
					'/fr/dokos/ressources-humaines/': getSideBar('ressources-humaines', 'Ressources Humaines', 'fr')
				}
			}
		}
	},
	plugins: [
		['@vuepress/active-header-links', true],
		['@vuepress/back-to-top', true],
		'@vuepress/nprogress',
		['@vuepress/medium-zoom', {
			selector: '.theme-default-content :not(a) > img',
			options: {
				margin: 16
			}
		}],
		[
			'@vuepress/google-analytics',
			{
				'ga': 'UA-35025071-5'
			}
		]
	]
}

function getSideBar(folder, title, lang, subfolders=[]) {
	const extension = [".md"];
	const frPath = path.join(`${__dirname}/../fr/dokos/${folder}`)
	const enPath = path.join(`${__dirname}/../dokos/${folder}`)

	const folderPath = lang == "fr" ? frPath : enPath;
	let files = fs
		.readdirSync(folderPath)
		.filter(
			(item) =>
			item.toLowerCase() != "readme.md" &&
			fs.statSync(path.join(folderPath, item)).isFile() &&
			extension.includes(path.extname(item))
		);

	subfolders.length&&subfolders.forEach(f => {
		const suFolderPath = path.join(folderPath, f)
		fs
			.readdirSync(suFolderPath)
			.filter(
				(item) =>
				item.toLowerCase() != "readme.md" &&
				fs.statSync(path.join(suFolderPath, item)).isFile() &&
				extension.includes(path.extname(item))
			).forEach(v =>
				files.push(`${f}/${v}`)
			)
	})

	return [{ title: title, children: ["", ...files] }];
}