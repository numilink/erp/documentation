# Payment entry

A payment entry is a document recording a payment made against a party.

### 1. How to create a payment entry

1. In a submitted but unpaid sales order, sales invoice or purchase invoice click on 'Make > Payment entry'
2. Check the paid amount 
3. Check the allocation to the reference document (sales order, sales invoice, etc...)
4. Add a reference and a reference date
5. Save and submit

### 2. How to register fees associated to a payment

The deductions or loss table allows us to register any type of fees associated to a payment.

:::tip Example
A purchase invoice of $40 has been posted for an amount of 36€ in the company currency.
The payment, several days later, is from 37€.
In the payment entry, the amount paid will be of 37€, the amount allocated to the invoice of 36€ and in the deduction table we add the following line:

Account: Foreign exchange loss
Amount: 1€
:::


:::tip Example
A sales invoice of 1000€ has been paid via GoCardlees, the amount really received is therefore 998€.
The amount paid in the payment entry is therefore 998€.
The amount allocated to the invoice is 1000€.
In the deduction table, we add the following line:

Account: GoCardless fees
Amount: 2€
:::