# Item booking


**Item booking is a flexible document for registering timeslots booking for an Item.**

### 1. Usage

You can use the Item Booking document type in two ways:
1. You can make a new booking manually through the desk.
2. You can configure the Item Booking document to allow your website users to book a slot themselves.


### 2. Prerequisites

#### 2.1. Venue settings

##### Minute UOM

If you want to allow guests to select an available slot on your website, you need to configure a unit of measure corresponding to one minute in the "Item Booking" section of the venue settings:
All slots being multiples of 1 minute, it is necessary to define it for dokos.

##### Enable simultaneous booking

Option to autorize simultaneous item bookings (several bookings of the same item during the same timeslot).
It activates the possibility to set a number of allowed simultaneous bookings for each item in the item master data.

##### Clear bookings in shopping cart after x minutes

Setup to define the time interval between the last modification of an item booking with status "In Cart" and its automatic deletion by the software. Set 0 to deactivate this functionality.

#### 2.2. Unit of measure

In order for the system to be able to convert the sales unit of measure into minutes to calculate the slots available, you need to configure one or several unit of measure conversion factors.

In the document type UOM conversion factor, check that you have a conversion factor for all your sales and stock UOM for one minute:
|From     |To    |Value           |
|---------|------|----------------|
|Sales UOM|Minute|Minute/Sales UOM|

![Time conversion to minutes](/images/venue/item_booking/time_conversion.png)

:::tip Example
If you plan to sell hourly and daily slots, make sure you have at least these two conversion factors:

|From|To    |Value|
|----|------|-----|
|Hour|Minute|60   |
|Day |Minute|480  |

Please note that here, 1 day corresponds to 480 minutes because we consider that, from a sales perspective, we sell 8 hour days.
You can, of course, adjust this value according to your business needs.

:::

#### 2.3. Item Booking Calendar

You can configure as many calendars as necessary per items and units of measure.
The calendar selection rule for an item booking is as follow:

1. __Dokos__ looks for a calendar linked with the item and the unit of measure requested
2. __Dokos__ looks for a calendar linked with the requested item, without linked unit of measure
3. __Dokos__ looks for a calendar linked to the requested unit of measure, without linked item
4. __Dokos__ looks for a calendar that is linked neither to item nor the requested unit of measure

It is therefore useful to configure at least one calendar, not linked to any item nor unit of measure, that will be considered the default calendar.

#### 2.4. Item

In order to allow the booking of timeslots on your website, you need to first show this item on your website: in the __Website__ section, select __Show in Website__.  
Enabling this option will allow you to also __Enable Item Booking__.

Once enabled, your website users will be given a choice between buying units or selecting a slot:
![Item booking options in website](/images/venue/item_booking/item_website_options.png)

You can uncheck the field __Disabled unit purchasing__ to allow only the booking of timeslots.

__Simultaneous bookings allowed__: Set the number of allowed simultaneous bookings (After activation in the venue settings)

If you want to autorize bookings with different unit of measure (day, hour,...), add them in the unit of measure table.
Please note that the conversion for bookings will be done against conversion factors for one minute, as setup above.

### 3. Slot booking

Your website users will be presented with a popup listing all available slots.
They can click on an available slot to add it to their shopping cart.


![Booking dialog](/images/venue/item_booking/item_booking_dialog.png)


Draft bookings are automatically deleted every x minutes (defined in venue settings) in case user abandon their shopping cart.

### 4. Link item bookings and events

If you organize events that yu want to manage from the "Event" calendar, it is possible to link your item bookings and your events.  

> You are organizing a temporary exhibition and need to book your main hall, the kitchen and two rooms during the whole duration of the event.
> Create an event and book your items from the "Book item" button on the Event document.
> You can then change the start/end time and booking conditions individually for each booked item.

#### 5. Portal

You can activate the portal "Bookings" to give an access to your clients to the list of timeslots they have booked.
The different bookings will appear with the following statuses: "Confirmed", "Cancelled", "Past".

If you give your clients the permission to write to an item booking, they will also see a button `Cancel` allowing them to cancel their bookings.
This permission can be given by checking the `Write` permission for the `Customer` role (default role) in the the Roles and Permissions Manager.

#### 6. Google Calendar Integration

It is possible to synchronize your item bookings with Google Calendar.
To setup the connection and create calendars, read this [section](/dokos/setting-up/integrations/google).

##### 6.1. Setup

You can link it each item with a calendar with **Item Booking** as reference document in the **Google Calendar** section of each item.
If an item is not linked to a calendar, you must select it manually in your booking document.

:::warning Warning
Each item must be linked with a different calendar.
:::

##### 6.2. Synchronization

To synchronize your bookings, check the box **Synchronize with Google Calendar**.
Check that a calendar is selected in the **Google Calendar** field.

:::warning Warning
__dokos__ doesn't handle exceptions linked to recurring events yet.
If you want to delete a recurring event in Google Calendar, make sure to select **All events**.
If you delete only one occurence of this event, it will not be reflected in __dokos__.
:::

To automate the synchronization of online bookings, check **Automatically synchronize with Google Calendar** in **venue settings**.  

If you want your item bookings to be always synchronized by default, customize the item booking form and add 1 as default value for the field **Synchronize with Google Calendar**.  
