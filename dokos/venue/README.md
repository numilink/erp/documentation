# Venue Module

The venue module has been created to group the main functionnalities to manage a venue.  

# Table of content

- [Item booking](/dokos/venue/item-booking.md)

- [Booking Credits](/fr/dokos/venue/booking-credits.md)