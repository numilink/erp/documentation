---
tags: ["configure", "security"]
---
# Configuring dokos

Once you have installed _Dokos_ you may want to activate its DNS Multitenant functionality and secure your website with [Let's encrypt](https://letsencrypt.org).

## DNS Multitenant

### Port based multitenancy

By default you new site is assigned to a specific port on your server.
The first one will be assigned to port 80, the second one to 81, etc.

To change the port assigned to a specific site, you can run:
`bench set-nginx-port {your site} {port number}`

For example:
`bench set-nginx-port examplesite.com 81`


### DNS based multitenancy

In order to simplify the management of your web server, you can activate the DNS Multitenant functionality.
This functionality allows you to simply name the folder containing your site like your domain name and let _Dodock_ handle the rest:

To activate the DNS Multitenant mode, launch:  
`bench config dns_multitenant on`

If you have already created a site, you can rename it with:  
`mv sites/{old site name} sites/{new site name}`

### Create a new site

To create a new site, you can launch:  
`bench new-site {your site}`

### Reload the web server

Once you have changed the port, activated the DNS Mulitenant mode or created a new site, you need to refresh the configuration for Nginx (web server).

1. Regenerate the config file: `bench setup nginx`
2. Reload Nginx: `sudo systemctl reload nginx` or `sudo service nginx reload`


## Let's encrypt

You can bring your own certificate to secure your _Dokos_ installation or generate a new certificate using Let's encrypt

### Prerequisites

1. Your bench should be in DNS Multitenant mode
2. Your web domain should redirect to your site's server
3. You need to be able to execute the commands with root permissions

### Generate a new certificate

`sudo -H bench setup lets-encrypt {your site}`

### Generate a new certificate for a custom domain

`sudo -H bench setup lets-encrypt {your site} --custom-domain {your custom domain}`

### Renew a certificate

Each Lets Encrypt certificate is valid for 3 months. You can renew them 30 days before they expire.
The generation of a new domain automatically creates a cron job that attempts to renew your certificate automatically.
If this renewal is not successful, you can still renew your certificate manually running:

`sudo bench renew-lets-encrypt`
