# Supplier

A supplier is a physical or legal entity from whom you buy products or services.

## 1. Create a supplier

> Buying > Supplier

A supplier is a master data requiring no pre-requisite.

### 1.1. Default settings

In order to facilitate your suppliers creation, it is possible to define some default information in **Buying Settings**.

1. **Naming system**: Each supplier has a unique identifier, the name of the document in which the supplier information are recorded. This supplier identifier can be based on the supplier full name or on a naming series.
If you choose to use the supplier full name as a basis, in case of homonyms, _Dokos_ will automatically add a "-1" at the end of the new supplier's name to differenciate them. 
If you choose to use a series, you can defin the prefix of this series in the document type **Naming Series**.

2. **Default supplier group**: Select the defaut supplier group. It is usually the most commonly selected supplier group.

3. **Default buying price list**: Set the price list selected by default when you create a new supplier.


### 1.2. Features

The information recorded in the supplier document are used as default values in transactions.
For example, if you define the default currency as "USD" in the supplier master data, the documents generated for this supplier (orders, invoices, ...) will be generated in US Dollars. You can always modify this value in the corresponding document while creating it.