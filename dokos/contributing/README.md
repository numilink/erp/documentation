# How to contribute

- Answer questions on the community [Forum](https://community.dokos.io)
- Report issues linked to the software on [Gitlab](https://gitlab.com/dokos/dokos/issues)
- [Translate](/dokos/contributing/translations) the software in your language