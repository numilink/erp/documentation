# V1.4.0

:tada: Read the release [blog post](https://dokos.io/en/blog/new-functionalities-version-1-4) :tada:

## dokos

#### New features
- New depreciation option: Prorated linear over 360 days
- The demo can now be generated in french and english


#### Major Bug corrections
- Correction of the auto update of subscription when paid via Stripe or GoCardless


## dodock [Framework]

#### New features
- New Map view available if the Google Maps integration is activated
- Every time a document is renamed this information is now tracked for audit purposes

#### Major Bug corrections
- Fixes in the email composer
- Fixes in the graphical Jinja composer in email templates
