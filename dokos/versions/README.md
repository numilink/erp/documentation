# Release notes

## Version 1

- [v1.0.0](/dokos/versions/v1_0_0.md)
- [v1.1.0](/dokos/versions/v1_1_0.md)
- [v1.2.0](/dokos/versions/v1_2_0.md)
- [v1.3.0](/dokos/versions/v1_3_0.md)
- [v1.4.0](/dokos/versions/v1_4_0.md)

## Version 2

- [v2.0.0](/dokos/versions/v2_0_0.md)