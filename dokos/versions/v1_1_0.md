# V1.1.0

:tada: Read the release [blog post](https://dokos.io/en/blog/new-functionalities-version-1-1) :tada:

## dokos

#### Features
- Possibility make half day leave applications
- New settings for DATEV export
- Possibility to allow simultenous item bookings
- Item booking draft clearing period is now configurable
- Item booking list per user is now available on the portal
- Specific address template for France (For new installations only. Existing installation please check the documentation)
- Possibility to add dynamic values (in Jinja) in contract templates

#### Bug corrections
- Permissions issues for subscriptions generated from webhooks
- Issue while fetching lead data in opportunities and quotations
- Shopify connector corrections
- Shopping cart addresses issues
- Addition of slots with two different unit of measure in the shopping cart
- Translation for "variant of" in shopping cart
- New message "Please select a unit of measure " when none is selected

#### Breaking change
- Payment gateway account message has been replaced with an email template at Payment Gateways Template level for payment requests.


## dodock [Framework]

#### Features
- [Server scripts](/dokos/setting-up/customization/server-scripts)
- Centralized notification center
- Configurable list view
- Unhandled emails older than 30 days are now deleted every night
- Possibility to make section breaks in print formats

#### Bug corrections
- Activity heatmap correction
- User creation from contact emails were sent twice
- Desk performance issue due to disabled caching
- Issues with currency formatting
- Auto removal of special characters for website links creation
- Possibility to set column width in % in print formats

#### Breaking change
- Removal of `jenv` hook

## Integrations

:tada: New [Zapier](https://zapier.com) integration available in beta version.  
Drop us an email at [hello@dokos.io](mailto:hello@dokos.io) to be invited.