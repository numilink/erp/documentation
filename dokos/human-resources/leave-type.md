# Leave Type

Leave types are used to configure the calculation rules for each leave type applicable to your company.  

## Options

- **Max Leaves Allowed**: Maximum number of leaves allowed for this type of leaves.
- **Applicable After (Working Days)**: This type of leaves is applicable only after x working days from the date of joining the company.
- **Maximum Continuous Days Applicable**: Maximum number of consecutive leave days an employee can apply for.
- **Is Carry Forward**: If checked, the balance leaves of this Leave Type will be carried forward to the next allocation period.
- **Is Leave Without Pay**: The salary will be deducted for this leave type.
- **Is Optional Leave**: Optional Leaves are holidays that Employees can choose to avail from a list of holidays published by the company. The Holiday List for Optional Leaves can have any number of holidays, but you can restrict the number of such leaves by setting the Max Days Leave Allowed field.
- **Allow Negative Balance**: If checked, employees will be allowed to apply for this leave type even if they don't have a sufficient balance. This will result in a negative balance for this type of leaves.
- **Include holidays within leaves as leaves**: Check this option if you wish to count holidays within leaves as a ‘leave’. For example, if an Employee has applied for leave on Friday and Monday, and Saturday and Sunday are weekly offs, if this option is checked, the system will consider Saturday as Sunday as leaves too. Such holidays will be deducted from the total number of leaves.
- **Exclude from leave acquisition**: If checked, all leave days taken with this leave type will be considered as absent when calculating the attendance for acquired leaves calculation.
- **Compensatory leave**: Compensatory leaves are leaves granted for working overtime or on holidays, normally compensated as an encashable leave. You can check this option to mark the Leave Type as compensatory. An Employee can request for compensatory leaves using Compensatory Leave Request.

## Pre-defined leaves

If the number of leaves is pre-defined, simply define the maximum number of a leaves allowed and the application rules.

## Acquired leaves

If leaves are acquired, select the acquisition rule in the corresponding section.  
If you select a period and a rounding rule, leaves will be calculated without taking into account the employee's attendance.  

Some countries have custom calculation formula adapted to local rules.  

### France

#### Standard leaves over 6 days

The formula takes into account 6 workdays weeks and divides the number of worked days by 24.  
The number of annual leaves is multiplied by the highest value between the number of worked days divided by 24 or the number of worked weeks divided by 4.  
If you select this formula, don't add saturday (or sunday) to the holiday list associated with your employee.  

#### Standard leaves over 5 days

The formula takes into account 5 workdays weeks and divides the number of worked days by 20.  
The number of annual leaves is multiplied by the highest value between the number of worked days divided by 24 or the number of worked weeks divided by 4.  
If you select this formula, add saturday and sunday (or any other week-end days) to the holiday list associated with your employee.  
  

## Encashable leaves

If this type of leaves can be encashed by the employee, check the corresponding the checkbox and define an encashment threshold.  

The encashment threshold is number of days an employee will not be able to encash. It will be only able to encash the days above this threshold.  
