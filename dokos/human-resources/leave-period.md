# Leave Period

A leave period allows the definition of start and end dates for the calculation of leaves for a particular leave type.  

:::tip
Standard leaves are calculated from 06-01 to 05-31 and compensatory leaves from 01-01 to 12-31 in France.  
Create to different leave periods for these two different leave types.  
:::

## Leave allocation

Once the dates defined and your document registered, click on the button "Grant leaves", add filters if applicable and click on "Grant".
If you don't add any filters, all employees will be automatically selected.  
New "Leave allocation" will be automatically created for all employees corresponding to selected filters.  

For acquired leaves, the allocation will be 0 and the calculation will start the following night.  