# Leave Policy

A leave policy defines the rules applicable to a category of employees.  

## Leave allocation

In the allocation table, add a new line for each leave type and define the annual leave allocation applicable for this leave type.  

## Links

The leave policy can be affected to each employee directly or to a grade.  
If you affect it to a grade, don't forget to assign this grade to all applicable employees.  