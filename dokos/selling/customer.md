# Customer

A customer is a physical or legal entity buying your products or services.

## 1. Create a customer

> Selling > Customer

A customer is a master data requiring no pre-requisite. It is however possible to create a customer from a [lead](/dokos/selling/lead) already existing in _Dokos_.

### 1.1. Default settings

In order to facilitate your customers creation, it is possible to define some default information in **Selling Settings**.

1. **Naming system**: Each customer has a unique identifier, the name of the document in which the customer information are recorded. This customer identifier can be based on the customer full name or on a naming series.
If you choose to use the customer full name as a basis, in case of homonyms, _Dokos_ will automatically add a "-1" at the end of the new customer's name to differenciate them. 
If you choose to use a series, you can defin the prefix of this series in the document type **Naming Series**.

2. **Customer group**: Select the defaut customer group. It is usually the most commonly selected customer group.

3. **Default Territory**: Select the territory that will be selected by default for a new customer.


### 1.2. Features

The information recorded in the customer document are used as default values in transactions.
For example, if you define the print language as "en" in the customer master data, the documents generated for this customer (quotations, sales invoices, ...) will be printed by default in english. You can always modify this value in the corresponding document while creating it.