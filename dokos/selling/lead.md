# Lead 

A lead is a natural or legal person presenting an interest in the goods or services you are selling.


# 1. Create a lead

> CRM > Lead

A lead is a master data requiring no pre-requisite.


# 2. Functionalities

## 2.1. Organization or natural person

If the lead is legal entity (company), you can check the checkbox "Lead is an organization".
In this case, the name of the organization must be filled. You can then link as many contacts as you wish to this lead.

If the lead is a natural person, you just need to enter his/her name in the corresponding field.

Once your lead registered, in the section "Address & Contact& you can add one or several addresses linked to this lead and one or several contacts.
If the field "Person Name" is filled, a contact is automatically created for this person.
You can also add an address and contact information in the corresponding section before the first save. They will be used to create the linked contact and address. 

You can also add several interlocutors to your lead during the sales cycle.

## 2.2. Follow Up

By filling in information regarding the next contact with this lead, an event is automatically added to your calendar. It will send you an email reminder the morning of the event.
You can also configure notifications based on your specific needs.


## 2.3 Additional information

You can add additional information on your lead in the notes, but also in the structured information field that can be easily used in reports to analyze you leads: market segment, website, industry, territory.
These information will be automatically copied at the creation of a customer from this lead.

The field "Unsubscribed" is automatically updated when the lead unsubscribes from the emails you send him/her. It is also used a filter for prospects you add to a new email group.
