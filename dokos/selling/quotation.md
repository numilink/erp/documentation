# Quotation

**A quotation is an estimated cost of the products/services you're selling to your future/present customer.** 

To access the Quotation list, go to:
> Selling > Quotation

A Quotation contains details about:

  * The recipient of the Quotation
  * The Items and quantities you are offering.
  * The rates at which they are offered.
  * The taxes applicable.
  * Other charges (like shipping, insurance) if applicable.
  * The validity of contract.
  * The time of delivery.
  * Other conditions.

:::tip
Images look great on Quotations. Make sure your items have an image attached.
:::

## 1. Prerequisites
Before creating and using a Quotation, it is advised that you create the following first:

* [Customer](/dokos/selling/customer)
* [Lead](/dokos/selling/lead)
* [Item](/dokos/stock/item)

## 2. How to create a Quotation
1. Go to the Quotation list, click on New.
2. Select if the Quotation is to a Customer or a Lead from the 'Quotation To' field.
3. Enter the Customer/Lead name.
4. Enter a valid till date after which the quoted amount will be considered invalid.
5. Order type can be Sales, Maintenance, or Shopping Cart. Shopping Cart is for website shopping cart and is not intended to be created from here.
6. Add the Items and their quantities in the items table, the prices will be fetched automatically from Item Price. You can also fetch items from an Opportunity by clicking on `Get Items from > Opportunity`.
7. Add additional taxes and charges as applicable.
8. Save.

You can also create a Quotation from an Opportunity by clicking on the button `Create > Quotation`.

## 3. Features

### 3.1 Address and Contact
In this section there are four fields:

* **Customer Address:** This is the billing address of the customer.
* **Shipping Address:** Address where the items will be shipped to.
* **Contact Person:** If your customer is an organization, then you can add the person to contact in this field.
* **Territory:** Region where the customer belongs to. Default is All Territories. 

### 3.2 Currency and Price List
You can set the currency in which the quotation/sales order is to be sent. 

Item prices will be fetched from the price list selected in the Price list field.  
The price list is automatically fetched first from the customer, then the customer group and finally the company if not set.
Ticking on Ignore Pricing Rule will ignore the Pricing Rules that may apply to this quotation.

### 3.3 The Items Table
This table can be expanded by clicking on the inverted triangle present rightmost of the table.

* On selecting an Item Code, the following fields will be fetched automatically: item name, description, any image if set, quantity default as 1, the rates. You can add discounts in the Discounts and Margin section. 
* **Under Discount and Margin** you can add extra margin for profit or give a discount. Both can be set based on either amount or percentage. The final rate will be shown below in the Rate section. You can assign an Item Tax Template created specifically for an item.
* **Item weights** will be fetched if set in the Item master.
* In **Warehouse and Reference**, the warehouse will be fetched from the Item master, this is the warehouse where your stock is present.
* Under **Planning** you can see the Projected quantity and the actual quantity present. If you click on the 'Stock Balance' button, it'll take you to a doctype where you can generate a stock report for the item.
* **Shopping cart**, additional notes is for website transactions. Notes about the item will be fetched here when added via a shopping cart. For example: make food extra spicy
* **Page Break** Will create a page break just before this item when printing.

* You can insert rows below/above, duplicate, move, or delete rows in this table.

:::tip
You can also download the items table in CSV format and upload it to another transaction.
:::

The total quantity, rate, and net weight of all items will be shown below the item table.

### 3.4 Taxes and Charges
To add taxes to your Quotation, you can select a [Sales Taxes and Charges Template](/dokos/selling/tax-template) or add the taxes manually in the Sales Taxes and Charges table.

The total taxes and charges will be displayed below the table. Clicking on Tax Breakup will show all the components and amounts.

You can add a [Shipping Rule](/dokos/selling/shipping-rule) here for the items in the quotation.

### 3.5 Additional Discount
Other than offering discount per item, you can add a discount to the whole quotation in this section.  
This discount could be based on the Grand Total i.e., post tax/charges or Net total i.e., pre tax/charges.  
The additional discount can be applied as a percentage or an amount.

### 3.6 Payment Terms
Sometimes payment is not done all at once. Depending on the agreement, half of the payment may be made before shipment and the other half after receiving the goods/services. You can add a Payment Terms template or add the terms manually in this section.

### 3.7 Terms and Conditions
You can select a Terms and Conditions template and modify it based on the specifics of this transaction.

### 3.8 Print Settings
#### Letterhead
You can print your quotation/sales order on your company's letterhead.

'Group same items' will group the same items added multiple times in the items table. This can be seen when your print.

#### Print Headings
Quotations can also be titled as “Proforma Invoice” or “Proposal”.
You can do this by selecting a **Print Heading**. 
To create new Print Headings go to:
> Settings > Printing > Print Heading.

### 3.9 More Information
* **Campaign:** A Sales campaign can be associated with the quotation. A set of quotations can be part of a sales campaign.
* **Source:** A Lead Source type can be linked if quoting to a lead, whether from a campaign, from a supplier, an exhibition etc,. Select Existing Customer if quoting to a customer.
* **Supplier Quotation:** A Supplier Quotation can be linked for comparing with your current quotation to a buyer. You can get an idea of profit/loss by comparing the two.

### 3.10 Submitting the Quotation
Quotation is a “submittable” transaction. When you click on save, a draft is saved, on submitting, it is submitted permanently. Since you send this Quotation to your Customer or Lead, you must freeze it so that changes are not made after you send the Quotation.

On submitting, you can create a Sales Order or an Auto Repetition from the Quotation using the Create button. In the Dashboard present on the top, you can go to the Sales Order linked with this Quotation.  
In case it didn't work out, you can set the Quotation as lost by clicking on the 'Set as Lost button'.